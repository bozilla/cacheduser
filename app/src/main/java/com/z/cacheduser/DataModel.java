package com.z.cacheduser;

/**
 * Created by Z on 4/11/2017.
 */

public class DataModel {
    public int icon;
    public String name;

    public DataModel(int icon, String name) {
        this.icon = icon;
        this.name = name;
    }
}
