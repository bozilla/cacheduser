package com.z.cacheduser;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    private static final int SHOWN = View.VISIBLE;
    private static final String EMPTY = "N/A";

    EditText userNameEt, passwordEt;
    LoginButton loginButton;
    TextView textView, pref;
    CallbackManager callbackManager;
    Button testDbBtn, savePrefBtn, showPrefBtn, clearPrefBtn;

    private String[] mNavigationDrawerItemTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    Toolbar toolbar;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);
        userNameEt = (EditText) findViewById(R.id.etUserName);
        passwordEt = (EditText) findViewById(R.id.etPassword);
        pref = (TextView) findViewById(R.id.pref);
        testDbBtn = (Button) findViewById(R.id.btnLogin);
        savePrefBtn = (Button) findViewById(R.id.savePref);
        showPrefBtn = (Button) findViewById(R.id.btnShowPref);
        clearPrefBtn = (Button) findViewById(R.id.btnClearPref);

        loginButton = (LoginButton) findViewById(R.id.fb_login_bn);
        textView = (TextView) findViewById(R.id.textView);
        callbackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                textView.setText("Login Success \n" + Profile.getCurrentProfile().getFirstName() + "\n"+loginResult.getAccessToken().getUserId() + "\n" + loginResult.getAccessToken().getToken());
                savePref(loginResult.getAccessToken().getUserId(), loginResult.getAccessToken().getToken());
            }

            @Override
            public void onCancel() {
                textView.setText("Login cancelled");
            }

            @Override
            public void onError(FacebookException error) {
                textView.setText("Error");
            }
        });

        // NEW

//        LoginManager.getInstance().registerCallback(callbackManager,
//                new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(LoginResult loginResult) {
//                        textView.setText("Login Success!! \n" + loginResult.getAccessToken().getUserId() + "\n" + loginResult.getAccessToken().getToken());
//                        savePref(loginResult.getAccessToken().getUserId(), loginResult.getAccessToken().getToken());
//
//                    }
//
//                    @Override
//                    public void onCancel() {
//                        textView.setText("Login cancelled");
//                    }
//
//                    @Override
//                    public void onError(FacebookException exception) {
//                        textView.setText("Error");
//                    }
//                });




        // END NEW
        loadPref();

        mTitle = mDrawerTitle = getTitle();
        mNavigationDrawerItemTitles= getResources().getStringArray(R.array.navigation_drawer_items_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        setupToolbar();

        DataModel[] drawerItem = new DataModel[3];

        drawerItem[0] = new DataModel(R.drawable.connect, "Connect");
        drawerItem[1] = new DataModel(R.drawable.fixtures, "Fixtures");
        drawerItem[2] = new DataModel(R.drawable.ic_settings_black_24dp, "Table");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);

        DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(this, R.layout.list_view_item_row, drawerItem);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        setupDrawerToggle();
        init();
        selectItem(0);
    }

    public void init()
    {
        userNameEt.setVisibility(SHOWN);
        passwordEt.setVisibility(SHOWN);
        pref.setVisibility(SHOWN);
        textView.setVisibility(SHOWN);
        testDbBtn.setVisibility(SHOWN);
        showPrefBtn.setVisibility(SHOWN);
        savePrefBtn.setVisibility(SHOWN);
    }

    public void savePref(String userName, String password)
    {
        SharedPreferences loginData = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = loginData.edit();
        editor.putString("userName", userName);
        editor.putString("password", password);
        loginButton.setVisibility(View.INVISIBLE);
        editor.apply();


    }



    public void onClearPref(View view)
    {
        SharedPreferences loginData = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = loginData.edit();
        editor.putString("userName", EMPTY);
        editor.putString("password", EMPTY);
        loginButton.setVisibility(View.VISIBLE);
    //TODO
//        clearPrefBtn.setVisibility(View.INVISIBLE);
        LoginManager.getInstance().logOut();
        editor.apply();
        selectItem(1);


//    clearPrefBtn.setVisibility(View.GONE);
    }

    public void onSavePref(View view) {
        savePref(userNameEt.getText().toString(), passwordEt.getText().toString());
    }

    public void onShowPref(View view) {
       loadPref();
    }

    private void loadPref()
    {
        SharedPreferences loginData = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        String name = loginData.getString("userName", EMPTY);
        String pw = loginData.getString("password", EMPTY);
        String msg = "Saved User Name: " + name + "\nSaved Password: " + pw;
        pref.setText(msg);
//        if(EMPTY.equals(name))
//        {
//            loginButton.setVisibility(View.VISIBLE);
//        }
//        else
//        {
//            loginButton.setVisibility(View.INVISIBLE);
//        }
    }

    public void onLogin(View view) {
        String userName = userNameEt.getText().toString();
        String password = passwordEt.getText().toString();
        String type = "login";
        BackgroundWorker bgWorker = new BackgroundWorker(this);
        bgWorker.execute(type, userName, password);
    }





    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }

    }



    private void selectItem(int position) {

        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = new ConnectFragment();
                setTitle(getString(R.string.connect));
                break;
            case 1:
                fragment = new FixturesFragment();
                setTitle(getString(R.string.fixtures));
                break;
            case 2:
                fragment = new TableFragment();
                setTitle(getString(R.string.table));
                break;

            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(mNavigationDrawerItemTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);

        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    private void setTitle(String title)
    {
        getSupportActionBar().setTitle(title);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    void setupToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    void setupDrawerToggle(){
        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this,mDrawerLayout,toolbar,R.string.app_name, R.string.app_name);
        //This is necessary to change the icon of the Drawer Toggle upon state change.
        mDrawerToggle.syncState();
    }


}
