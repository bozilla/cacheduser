package com.z.cacheduser;


import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Z on 3/18/2017.
 */

public class BackgroundWorker extends AsyncTask<String,Void,String>{

    Context context;
    AlertDialog alertDialog;

    BackgroundWorker (Context ctx) {
        context = ctx;
    }

    @Override
    protected String doInBackground(String... params) {
        String type = params[0];
        String user_name = params[1];
        String password = params[2];

        String login_url = "http://52.60.159.159/login.php";
        if(type.equals("login")) {

            try
            {
                URL url = new URL(login_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection) url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);

                OutputStream outputStream = httpUrlConnection.getOutputStream();
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("user_name", "UTF-8") +"=" +URLEncoder.encode(user_name, "UTF-8") +"&"
                        + URLEncoder.encode("password", "UTF-8") +"=" + URLEncoder.encode(password, "UTF-8");
                bw.write(post_data);
                bw.flush();
                bw.close();;
                outputStream.close();

                InputStream inputStream = httpUrlConnection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result = "";
                String line = "";
                while((line = br.readLine()) != null)
                {
                    result += line;
                }
                br.close();
                inputStream.close();
                httpUrlConnection.disconnect();
                return result;

            }catch(MalformedURLException e) {
                e.printStackTrace();
            } catch(IOException ioe)
            {
                ioe.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("Test DB Result");
    }

    @Override
    protected void onPostExecute(String result) {

        alertDialog.setMessage(result);
        alertDialog.show();

    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }


}
